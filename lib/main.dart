import 'package:flutter/material.dart';
import 'package:teleferico/Screems/LogIn.dart';
import 'package:teleferico/Screems/TelefericosList.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData.dark(),
      initialRoute: 'LogIn',
      routes: {
        'LogIn' : (BuildContext) =>LogInState(),
        'TelList' : (BuildContext) =>TelefericoList()
      },
      
    );
  }
}

