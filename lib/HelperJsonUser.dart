import 'dart:convert';

import 'package:teleferico/Objets/User.dart';
import 'package:http/http.dart' as http;


class HelperUser{

  String url = "https://jsonplaceholder.typicode.com/users";
  
  List<User> users = []; //lista auxiliar de Usuarios
  
  Future<List<User>> getUser() async{
    var data = await http.get(url);//creando peticion http.get en url de la api devuelve un future(response)

    var jsonData = json.decode(data.body); // recibiendo datos de tipo dynamic
    //List<User> users = []; //lista auxiliar de Usuarios  
    
    for(var tmpU in jsonData ){ //conversion de json ----J Usuarios
        User tmp = User(user: tmpU['email'],pass:tmpU['username']);
        users.add(tmp);
    }
    print(users.length);
    //print(verificationUser(name));
    return users;
  } 

  bool verificationUser(String user){
    for (var item in users) {
      if(item.user == user){
          return true;
      }
    }
    return false;
  }
}   