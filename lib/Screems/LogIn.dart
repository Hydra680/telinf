import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:teleferico/HelperJsonUser.dart';
import 'package:teleferico/Objets/User.dart';

class LogInState extends StatefulWidget {
  @override
  _LogInStateState createState() => _LogInStateState();
}

class _LogInStateState extends State<LogInState> {
  bool obscure = true;
  TextEditingController _email = new TextEditingController();
  TextEditingController _pass = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    _portraitModeOnly();
    return Scaffold(appBar: null, body: SafeArea(child: _body()));
  }
  //bloquear horientacion horizontal
  void _portraitModeOnly() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
  }

  Widget _listUser() {
    return ListView(
      physics: BouncingScrollPhysics(),
      children: <Widget>[
        Center(
            child: Text(
          "Mi Teleferico",
          style: TextStyle(color: Colors.blue, fontSize: 50),
        )),
        SizedBox(
          height: 50,
        ),
        TextField(
          controller: _email,
          keyboardType: TextInputType.emailAddress,
          maxLines: 1,
          decoration: InputDecoration(
              prefixIcon: Icon(Icons.email),
              labelText: "Usuario o Email",
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10.2)))),
        ),
        SizedBox(
          height: 15,
        ),
        TextField(
            controller: _pass,
            keyboardType: TextInputType.text,
            maxLines: 1,
            obscureText: obscure,
            decoration: InputDecoration(
                prefixIcon: IconButton(
                    icon: Icon(
                      Icons.lock,
                      size: 20,
                    ),
                    onPressed: () {
                      setState(() {
                        if (obscure) {
                          obscure = false;
                        } else {
                          obscure = true;
                        }
                      });
                    }),
                labelText: "Contraseña",
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.2))))),
        SizedBox(
          height: 15,
        ),
        RaisedButton(
            splashColor: Colors.blue,
            color: Colors.black,
            textColor: Colors.white,
            elevation: 5,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),
                side: BorderSide(color: Colors.blue)),
            child: Text("Sig In"),
            onPressed: () async {
              HelperUser h = HelperUser();
              List<User> users = await h.getUser();
              for (var item in users) {
                /*print("##################################################3");
                print(item.user);
                print(_pass.text.toString().trim());
                print("#######----------------------------------3##############");*/
                if (item.user == (_email.text.toString().trim())) {
                  print("yes checked...!");

                  Navigator.of(context).pushNamed('TelList');
                  break;
                } else {
                  print("checked fail...!");
                }
              }
            }
            //h.getUser("Sincere@april.biz");
            ),
        Container(
          padding: EdgeInsets.only(top: 10),
          child: Row(
            children: <Widget>[
              Flexible(flex: 2, child: Text("No Tienes Cuenta? ")),
              Flexible(
                  flex: 3,
                  child: FlatButton(
                    splashColor: Colors.white,
                    disabledColor: Colors.white,
                    focusColor: Colors.white,
                    hoverColor: Colors.white,
                    onPressed: () => _showDialogo(context),
                    child: Text(
                      "Desea Registrarse...",
                      style: TextStyle(color: Colors.blue, fontSize: 15),
                    ),
                  ))
            ],
          ),
        )
      ],
    );
  }

  void _showDialogo(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return AlertDialog(
            title: Text("Registrars"),
            shape: BeveledRectangleBorder(
                borderRadius: BorderRadius.circular(10.2)),
            contentPadding:
                EdgeInsets.only(top: 25, bottom: 25, left: 10, right: 10),
            content: SingleChildScrollView(
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    CircleAvatar(
                      radius: 80,
                      child: Icon(Icons.camera_roll),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    TextField(
                      keyboardType: TextInputType.emailAddress,
                      maxLines: 1,
                      decoration: InputDecoration(
                          prefixIcon: Icon(Icons.email),
                          labelText: "Usuario",
                          border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.2)))),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    TextField(
                      keyboardType: TextInputType.emailAddress,
                      maxLines: 1,
                      decoration: InputDecoration(
                          prefixIcon: Icon(Icons.email),
                          labelText: "Email",
                          border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.2)))),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    TextField(
                      keyboardType: TextInputType.emailAddress,
                      maxLines: 1,
                      decoration: InputDecoration(
                          prefixIcon: Icon(Icons.email),
                          labelText: "Password",
                          border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.2)))),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    TextField(
                      keyboardType: TextInputType.emailAddress,
                      maxLines: 1,
                      decoration: InputDecoration(
                          prefixIcon: Icon(Icons.email),
                          labelText: "Repita su Pasword",
                          border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.2)))),
                    ),
                  ],
                ),
              ),
            ),
            actions: <Widget>[
              RaisedButton(
                onPressed: () {},
                color: Colors.white,
                child: Row(
                  children: <Widget>[
                    Icon(Icons.check_circle_outline),
                    Text("Registrar")
                  ],
                ),
              ),
              RaisedButton(
                onPressed: () {},
                color: Colors.white,
                child: Row(
                  children: <Widget>[Icon(Icons.cancel), Text("Cancelar")],
                ),
              )
            ],
          );
        });
  }

  Widget _body() {
    return Container(
      padding: EdgeInsets.only(
          top: (MediaQuery.of(context).orientation == Orientation.portrait)
              ? 200
              : 0,
          right: 50,
          left: 50),
      child: _listUser(),
    );
  }
}
