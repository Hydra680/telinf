import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';

class CompraBoleto extends StatefulWidget {
  @override
  _CompraBoletoState createState() => _CompraBoletoState();
}

class _CompraBoletoState extends State<CompraBoleto> {
  bool qr = false;
  Color c = Colors.red;
  Map<String, Color> col = {
    'Linea Roja': Colors.red,
    'Linea Verde': Colors.green,
    'Linea Azul': Colors.blue,
    'Linea Celeste': Colors.lightBlue,
    'Linea Blanca': Colors.white
  };
  String dropdownValue = 'Linea Roja';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _body(),
    );
  }

  Widget _body() {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.all(15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Center(
              child: Text(
                "Compra De Entrada",
                style: TextStyle(color: Colors.blue, fontSize: 40),
              ),
            ),
            SizedBox(
              height: 15,
            ),
            Center(
                child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Saldo Disponible: ",
                  style: TextStyle(color: Colors.white, fontSize: 25),
                ),
                Text(
                  "00.0",
                  style: TextStyle(color: Colors.lightBlue, fontSize: 25),
                ),
              ],
            )),
            SizedBox(
              height: 15,
            ),
            Text("Seleccione la linea:"),
            Center(
              child: DropdownButton<String>(
                value: dropdownValue,
                icon: Icon(
                  Icons.arrow_downward,
                  color: c,
                ),
                iconSize: 24,
                elevation: 16,
                style: TextStyle(color: c),
                underline: Container(
                  height: 2,
                  color: c,
                ),
                onChanged: (String newValue) {
                  setState(() {
                    c = col[newValue];
                    dropdownValue = newValue;
                  });
                },
                items: <String>[
                  'Linea Roja',
                  'Linea Verde',
                  'Linea Azul',
                  'Linea Celeste',
                  'Linea Blanca'
                ].map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList(),
              ),
            ),
            Center(
                child: Container(
                    padding: EdgeInsets.all(15),
                    width: 250,
                    child: TextField(
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                          prefixIcon: Icon(Icons.format_list_numbered),
                          labelText: "cantidad",
                          border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.2)))),
                    ))),
            Center(
              child: RaisedButton(
              color: Colors.black,
              shape: StadiumBorder(),
              onPressed: () {
                setState(() {
                  qr = true;
                  print(qr);
                });
              },
              child: Text(
                "Comprar Entrada",
                style: TextStyle(color: Colors.white),
              ),
            )),
            SizedBox(
              height: 15,
            ),
            Center(
              child: RaisedButton(
                color: Colors.black,
                shape: StadiumBorder(),
                onPressed: () => _showDialogo(context),
                child: Text(
                  "lista De Boletos",
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),

            //
          ],
        ),
      ),
    );
  }

  void _showDialogo(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return AlertDialog(
              title: Text("Registrars"),
              content: SingleChildScrollView(
                child: Container(
                  child: Center(
                    child: Column(
                      children: <Widget>[
                        SizedBox(height: 15,),
                        FlutterLogo(size: 150,),
                        Text("Boleto Personal \ncomprada el dd/mm/yyyy"),
                        SizedBox(height: 15,),
                        FlutterLogo(size: 150,),
                        Text("Boleto 4 Perosnas \ncomprada el dd/mm/yyyy"),
                        SizedBox(height: 15,),
                        FlutterLogo(size: 150,),
                        Text("Boleto 5 personas \ncomprada el dd/mm/yyyy"),

                      ],
                    ),
                  ),
                ),
              ));
        });
  }
}
